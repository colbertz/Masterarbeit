\section{Wovon träumten die Menschen in der Steinzeit?}


\begin{flushright}{\slshape    
    Information ist Energie. Bei jeder Weitergabe verliert sie etwas davon.} \\
    \medskip --- Wolfgang Herbst \cite{zitate}
\end{flushright} 


%Was dachten und fühlten die Menschen vor 30.000 Jahren? 

%Woher wissen wir, was die Menschen vor 30.000 Jahren dachten und fühlten? 
%Woher sollen wir wisse, was die Menschen vor 30.000 Jahren dachten und fühlten?
Eine Zufallsentdeckung im Jahr 1940 in Frankreich gibt uns einen Eindruck davon,
was die Steinzeitmenschen für wichtig genug hielten, für die Nachwelt festgehalten zu
werden. Die Höhle von Lascaux - zufällig entdeckt von vier jungen Männern - ist
wegen ihrer äußerst gut erhaltenen und sehr beeindruckenden Höhlenmalereien weltberühmt geworden. Wir können heute diese
Bilder bewundern und auch teilweise verstehen. Doch viele der dargestellten
Szenen entziehen sich vollkommen unserem Verständnis. Dennoch ist zu
erkennen, dass der Mensch schon sehr früh das Bedürfnis hatte, seine
Erfahrungen und Gedanken zu bewahren.

Aber für den modernen Menschen stellen sich zahlreiche Fragen: Warum haben
unsere Vorfahren diese Bilder genau gemalt? Was genau stellen die Szenen dar?
Beschreiben die Zeichnungen eine Mythologie, die wir heute \NOT mehr kennen,
stellen sie Szenen aus dem Alltag der Menschen dar oder vielleicht sogar
deren Wünsche oder Träume?

Wir kennen weder den Glauben, noch das Leben oder die Gesellschaft dieser
Menschen gut genug, um die Bilder alle verstehen zu können. Ebenso fehlen uns
Kenntnisse über ihre Umwelt und Lebensumstände, die mit Sicherheit in die
Zeichnungen miteingeflossen sind. Die Hauptprobleme der Informationsbewahrung,
die Haltbarkeit der Materialien und die Semantik der Informationen, sind auch in
den folgenden Jahrtausenden \NOT gelöst worden.

Lange Zeit hatte der moderne Mensch ähnliche Probleme mit der Bildschrift einer
anderen, jüngeren Kultur. Im Alten Ägypten wurde von etwa 3.200 v. Chr. bis 394
n. Chr. das Schriftsystem der ägyptischen Hieroglyphen verwendet.
Die Hieroglyphen wurden bspw. in die Wände von Tempeln geritzt, so dass sie bis
in die Neuzeit erhalten geblieben und lesbar sind, jedoch lange Zeit ohne
jede Bedeutung für den modernen Menschen waren. Eine Entschlüsselung schien
unmöglich, bis während der Ägypten-Expedition Napoleons am 15. Juli 1799
der Stein von Rosette entdeckt wurde. Dieser Stein rückte die Bildsprache der
Alten Ägypter in einen historischen und sprachlichen Kontext, was einen ersten
Schritt zur Entschlüsselung dieser Schrift ermöglichte.

So herausfordernd die Entschlüsselung der Hieroglyphen sich auch gestaltete, so
hinterließen uns die Alten Ägypter zumindest Schriften, die wir entziffern
können. Ein ganz anderes Problem stellt sich bei der keltischen Kultur. Die
keltischen Druiden gaben ihre Kenntnisse fast ausschließlich mündlich an
ihre Schüler weiter. Während die Römer oder Ägypter viele ihrer Informationen so
festhielten, dass wir sie heute noch lesen können, sind die Kenntnisse der
Kelten für uns größtenteils für immer verloren und wir sind oftmals auf Quellen
Dritter oder Interpretationen von archäologischen Funden angewiesen. Und gerade
Quellen wie Cäsars \rede{De Bello Gallico} sind \NOT unbedingt durch Objektivität geprägt und die Interpretation von Funden kann natürlich
falsch sein.

Doch \NOT nur der Erhalt der reinen Informationen stellt ein Problem dar. Bei
Dokumenten aus dem Mittelalter bspw. tritt ein weiteres Problem zu Tage, mit dem
die Forscher der heutigen Zeit zu kämpfen haben: Zwar sind die Dokumente in mehr
oder weniger gutem Zustand erhalten und wir verstehen auch die Sprache, aber
die Semantik der beschriebenen Informationen ist \NOT mehr erkennbar.

Mittelalterliche Besitzurkunden können heute vielleicht noch gelesen
werden, doch die Interpretation wird dadurch erschwert, dass uns
Hintergrundwissen fehlt. Ein Problem ist, dass es \bspw keine klar geregelten
Adressen gab. Ein Grundstück könnte beschrieben werden als \rede{Die Wiese an
der Mühle}, doch die Mühle ist heute \NOT mehr erhalten und wir wissen \NOT,
wo sie genau stand. Derselbe Ort kann in verschiedenen Dokumenten
unterschiedlich benannt sein. Vielleicht ist die \rede{Wiese am Fluss} dasselbe
Grundstück, vielleicht auch \NOT.

Auch die Namen von Personen waren \NOT standardisiert: Eine Person konnte durchaus unter
verschiedenen Namen bekannt gewesen sein. Die Informationen, die diese Dokumente
für uns interpretierbar machen, fehlen meistens und müssen zusätzlich zur
Entschlüsselung des eigentlichen Dokuments noch mühsam erarbeitet werden.

Erschwerend kommt hinzu, dass das damals verwendete Material \NOT mehrere
Jahrhunderte lang haltbar ist und sich aufgrund seines Alters zersetzt. So
müssen teilweise aufwändige Konservierungstechniken entwickelt und angewendet
werden, um die Dokumente, die uns noch vorliegen, auch weiterhin erhalten und
erforschen zu können.

Unsere moderne Gesellschaft ist eine Gesellschaft, der in eine unglaublich große
Menge an Informationen erzeugt wird, mehr als jemals zuvor in der Geschichte
der Menschheit. Diese gewaltigen Datenmengen müssen \NOT nur langfristig
gespeichert werden, sondern auch auf lange Sicht verständlich bleiben. Die
Technik und der Bedarf der Menschen an Informationen haben sich verändert, doch
viele grundlegende Probleme sind noch \NOT gelöst. Der große Unterschied zu
den Kulturen der Vergangenheit liegt darin, dass der moderne Mensch wesentlich
mehr über die mit der langfristigen Speicherung von Informationen verbundenen
Probleme nachdenkt und nach Lösungen sucht.

Das Problem der Haltbarkeit ist mit den digitalen Medien \NOT gelöst.
Wissenschaftliche Untersuchungen haben ergeben, dass sogar \acp{CD} und
\acp{DVD} nur eine mittlere Lebensdauer von 20 - 30 Jahren besitzen und somit für eine
langfristige Speicherung \NOT geeignet sind. Damit vertrauen wir unsere Daten
weniger langlebigen Medien an als die Alten Ägypter, deren in Stein eingehauene
Hieroglypen noch heute sichtbar sind. Doch die gigantischen Datenmengen, die in
der heutigen Gesellschaft anfallen, erfordern auch Speichermedien mit einer
entsprechend riesigen Kapazität.

In vielen Bereichen spielt die Frage nach einer langfristigen
Speichermöglichkeit schon heute eine große Rolle. So müssen die Ergebnisse von
Forschungsprojekten über einen langen Zeitraum hinweg sicher und zuverlässig
aufbewahrt werden können, damit andere Forschungsprojekte auf den bereits gesammelten
Informationen aufbauen können. Auch Unternehmen, Behörden und Privatleute
besitzen eine große Menge an Daten, die sie nur ungerne durch technische
Defekte oder dadurch, dass Speichermedien das Ende ihrer Lebensdauer erreicht
haben, verlieren würden. 

Aber von der zuverlässigen Speicherung von Informationen können auch
Menschenleben in der Zukunft abhängen. Die französische Behörde für die
Entsorgung radioaktiven Abfalls \ac{ANDRA} lagert hochgiftigen Atommüll in
Fässern in riesigen Betonbunkern. Die Fässer müssen Hunderte oder sogar Tausende von Jahren
ungeöffnet bleiben. Doch wie kann sichergestellt werden, dass diese
lebenswichtige Information in der Zukunft noch gelesen werden kann?

Die \ac{ANDRA} verwendet ironischerweise im Moment Papier, um diese
Informationen festzuhalten. Modernes Papier weist eine deutlich längere Lebensdauer
auf als die digitalen Speicher, bringt aber weitere große Nachteile mit sich:
Gebundenheit an einen Standort, keine Mehrbenutzerfähigkeit, Gefahr der
Vernichtung aller Daten bspw. durch einen Brand. 

Doch selbst wenn die technischen Voraussetzungen gegeben sind, dass
Informationen über eine sehr lange Zeit sicher aufbewahrt werden können, muss
noch ein weiteres Problem gelöst werden, das auch bei den Höhlenmalereien oder
den Dokumenten aus dem Mittelalter auftritt: Können Leser in der Zukunft den
Sinn der Dokumente erfassen?
   
Im Beispiel des atomaren Abfalls stellt sich die Frage: Wie können wir
nachfolgenden Generationen mitteilen, dass sie diese Bunker und die darin
gelagerten Fässer unter keinen Umständen öffnen dürfen? Werden die Menschen in
der Zukunft unsere heutigen Symbole für Gefahr noch interpretieren können?
Werden sie überhaupt unsere Sprache noch verstehen können? Gibt es Zeichen, die
universell auf eine Gefahr hindeuten? Oder gibt es Möglichkeiten, diese
Information von Generation zu Generation weiterzugeben, vollkommen unabhängig
von den Gegegebenheiten der Zukunft, die wir noch \NOT kennen können?

Auf all diese Fragen gibt es bis zur heutigen Zeit noch keine Antworten. 
Verschiedene Forschungsprojekte arbeiten an diesen Problemen und entwickeln neue
Speichermedien, die bspw. auf Quarz oder sogar \ac{DNS} basieren. Ebenso wurden
Verfahrensmodelle entwickelt, welche die Prozesse und die notwendige
Organisation beschreiben, um Informationen sowohl technisch als auch semantisch
für eine sehr lange Zeit haltbar und interpretierbar zu machen. 

Die hier vorliegende Arbeit hat \NOT den Anspruch, eine Lösung für diese
Probleme zu erarbeiten. Sie baut auf den Erkenntnissen von Forschungsarbeiten
auf, die sich mit möglichen Lösungen beschäftigen, und beschreibt die
Entwicklung des Prototyps einer Software, die den Anwender bei der Verwaltung
von Daten unterstützen soll, die er für eine Langzeitarchivierung vorgesehen
hat.

\vspace{1cm}
Quelle für die Einleitung war \cite{amouroux.2014}