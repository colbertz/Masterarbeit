\subsection{Modul \code{Database}}
\label{sec:serverDatabaseModellierung}
Bei der Erstellung eines \INFPAKg fallen zahlreiche Daten an, die zwar auf dem
Server gespeichert werden sollen, für die aber aufgrund einer klaren Struktur
nicht der \TRIPSTORE verwendet werden soll. Hier wird eine traditionelle
relationale Datenbank verwendet. 

Das \MODDATA enthält alle \FUNKUNITp, die der Verwaltung der Daten in
der Datenbank gewidmet sind. Zu dem momentanen Zeitpunkt werden die folgenden
Daten in der Datenbank gespeichert:

\begin{itemize}
  \item Wesentliche Metadaten der Informationspakete und der digitalen Objekte,
  \item die aus den digitalen Objekten extrahierten Metadaten,
  \item die Klassen der Ontologien,
  \item die Sichten auf die Ontologien.
\end{itemize}

Dabei liegt das in Abbildung \ref{img:ermodell} dargestellte Datenbank-Schema
zugrunde. 
   
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.5]{graphics/ermodell.png}
	\caption{Das ER-Diagramm der Datenbank}
	\label{img:ermodell}
\end{figure}

Möchte der Benutzer eine Referenz zu einer lokal auf dem Server gespeicherten
Datei zu einem \INFPAK hinzufügen, muss erst bestimmt werden, ob diese Datei
bereits im \STORDIR des Servers vorliegt. Ist dies \NOT der Fall, wird die
Datei hochgeladen. Am einfachsten lässt sich die Existenz einer Datei mithilfe
einer eindeutigen Prüfsumme bestimmen, für deren Berechnung verschiedene
Verfahren angewendet werden können.

Der Ablauf ist dabei wie folgt: Der Anwender wählt eine Datei von seinem lokalen
Dateisystem. Die Prüfsumme $p_1$ wird berechnet und an den Server gesendet.
Der Server durchsucht das \STORDIR und berechnet von jeder vorhanden Datei die
Prüfsumme $p_2$. Sind die beiden Prüfsummen $p_1$ und $p_2$ identisch, ist die
Datei gefunden und die Suche kann beendet werden.  

Der Vorteil an diesem Verfahren ist, dass die Gleichheit von Dateien trotz
unterschiedlicher Dateinamen festgestellt werden kann. Außerdem können unterschiedliche Versionen einer Datei verwaltet
werden. Doch diese Lösung hat auch einen großen Nachteil: Wird die Suche
direkt auf dem Dateisystem ausgeführt, ist mit Performanzeinbußen
zu rechnen, da dies neben zeitaufwändigen Festplattenoperationen
auch die erneute Berechnung der Prüfsumme aller vorhandenen digitalen Objekte
bedeutet, bis die gesuchte Datei gefunden ist. 

Bei n Dateien im \STORDIR würde dies im besten Fall bedeuten, dass diese
Operation nur einmal ausgeführt werden muss, da die erste Datei direkt die
gesuchte ist. Im schlechtesten Fall, dass die gesuchte Datei überhaupt \NOT
existiert, müssten die Operationen und Berechnungen n mal durchgeführt werden. 

Wie in der Ist-Analyse \sectionReference{sec:istanalyse} beschrieben, wurden
im alten {\PACKAGINGTOOLKIT} Zusatzinformationen über die Archive im
Namen der \ZIPFILEp codiert. Diese Lösung ist auf Dauer jedoch nur schwer zu
handhaben. Sie ist fehleranfällig, da an verschiedenen Stellen im Programm
immer die Korrektheit der Dateinamen überprüft und sichergestellt werden muss.
Außerdem besteht die Gefahr, dass die Dateinamen mit ihren Pfadangaben mit
diesen zusätzlichen Informationen bald an die Längenbegrenzungen des
Betriebssystems stoßen. Es hätte zusätzlichen Programmieraufwand erfordert, um
auf die daraus entstehenden Probleme zu reagieren.

Eine Alternative wäre es, die Informationen in einer Textdatei zu
speichern. Bei dieser Lösung müsste jedoch ein Zusammenhang zwischen einem
\INFPAK und der Textdatei hergestellt werden. Dies könnte über die
\ac{UUID} des \INFPAKg geschehen, welche den Dateinamen bilden könnte.
Bei der Verwendung von Textdateien wird jedoch Flexibilität bzgl. der
Erweiterbarkeit eingebüßt. Da bei einer Erweiterung der in der Datei
gespeicherten Informationen entsprechender Code zum Lesen und Schreiben
implementiert werden muss, steigt auch die Fehleranfälligkeit und 
dementsprechend auch der Testbedarf.

Die Lösung, welche die meisten Vorteile verspricht, ist die Speicherung dieser
Informationen in einer Datenbank. Die oben beschriebene aufwendige Neuberechnung
der Prüfsummen entfällt. Die Prüfsumme einer Datei wird einmal berechnet, wenn
die Datei ins \STORDIR geladen wird, und wird anschließend in der Datenbank
gespeichert. Möchte der Benutzer eine weitere Datei hochladen, wird die
Prüfsumme dieser Datei berechnet. Mit einem einfachen Select-Kommando wird in
der Datenbank nach dieser Prüfsumme gesucht. Dabei werden die äußerst
effizienten Algorithmen und Datenstrukturen der Datenbank für die Suche
verwendet. Somit entfallen die oben beschriebenen Probleme und die Suche kann
erheblich schneller und effizienter durchgeführt werden. Zusätzlich wird in der
Datenbank festgehalten, welche \INFPAKp auf welche digitalen Objekte
referenzieren, um auch hier den Suchaufwand gering halten zu können.

Auch die Speicherung der zusätzlichen Metainformationen ist in einer Datenbank
erheblich effizienter und weniger fehleranfällig möglich als bei den beiden oben
beschrieben Ansätzen. Zusätzlich ist die Erweiterung um weitere Metadaten ohne
weiteres möglich, ohne an Grenzen zu stoßen.  Da zahlreiche Frameworks zur
Verwaltung von in einer Datenbank gespeicherten Informationen existieren, hält
sich der Programmieraufwand in Grenzen und es kann auf Lösungen gesetzt werden,
die sich in zahlreichen Projekten bewährt haben. Somit fällt auch der
Testaufwand geringer aus. Die Daten der \INFPAKp selbst werden repräsentiert
durch RDF-Triples, die von dem \MODTRIP verwaltet werden
\sectionReference{sec:serverModellierungTripleStore}. Somit ist dieses Modul Bestandteil
aller Anwendungsfälle, die sich auf die Verwaltung von Informationspaketen beziehen.

Die anderen Datenbanktabellen beziehen sich auf die Sichten auf die \BASEONT, die der
Benutzer definieren kann, um im Client-Programm Klassen aus der Ontologie
herausfiltern und so eine höhere Übersichtlichkeit zu erhalten (Anwendungsfälle 
19 -21).

Die Informationen zu den Sichten werden ebenfalls in der Datenbank abgelegt.
Dabei wird gespeichert, welche Klasse der \BASEONT welcher Sicht zugeordnet
ist. Somit ist sichergestellt, dass sämtliche Clients, die mit diesem Server
kommunizieren, auf dieselben Sichten zugreifen können. Eine spätere Auslagerung
dieses Moduls auf einen eigenen Datenbankserver wäre natürlich möglich.
