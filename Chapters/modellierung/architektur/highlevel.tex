\subsection{High-Level-Architektur} 
\label{sec:highlevel}
Im Folgenden soll ein Überblick über den grundsätzlichen
Aufbau der Anwendung dargestellt werden. Dabei werden die gröbsten Strukturen
identifiziert und die Kommunikation zwischen ihnen und nach außen beschrieben.
Die Architektur ist in \imageReference{img:highlevel} zu sehen.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.5]{graphics/highlevel.png}
	\caption{Die High-Level-Architektur der Anwendung}
	\label{img:highlevel}
\end{figure}     

{\PACKAGINGTOOLKIT} ist als verteiltes System in drei Komponenten
unterteilt, die jeweils ein unabhängiges Softwareprojekt darstellen: Client,
Server und Mediator.

Dabei werden sowohl die Architekturmuster Client-Server
\sectionReference{sec:clientServer} als auch Mediator-Wrapper
\sectionReference{sec:mediatorWrapper} eingesetzt. Jede Komponente hat klar definierte
Aufgaben, die sich sowohl aus den Architekturmustern als auch den Anforderungen 
ergeben \sectionReference{sec:sollkonzept}.

Zur Erfüllung dieser Aufgaben müssen Daten zwischen den Komponenten ausgetauscht
werden. Dabei sind die Komponenten so weit unabhängig voneinander, dass sie auf physisch
getrennten Maschinen ausgeführt werden können.

Prinzipiell folgen alle drei Komponenten einem ähnlichen Aufbau: Sie sind in
Schichten eingeteilt \sectionReference{sec:schichtenmodell}, wobei jede Schicht eine
ganz klare Aufgabe hat. Die oberste Schicht übernimmt die Kommunikation mit den anderen Komponenten bzw. mit
dem Benutzer. Die unterste Schicht enthält atomare Operationen, die \NOT
weiter unterteilt werden können. Die mittlere Schicht koordiniert die beiden
anderen Schichten, \dahe sie verbindet die atomaren Operationen zu
Geschäftslogik und bereitet die Ergebnisse der Geschäftslogik so auf, dass die
oberste Schicht mit ihnen arbeiten kann.

Der Client stellt in der obersten Schicht die grafische Oberfläche dar, über welche der Anwender das
Programm bedient. Die View-Schicht enthält nur die Logik, die direkt mit der Darstellung der
Benutzeroberfläche zu tun hat. Für alle weiteren Aufgaben ruft die View-Schicht
Prozeduren der Service-Schicht auf. Diese wiederum verbindet mehrere Aufrufe von
Prozeduren der Operations-Schicht zu logischen Abläufen. Somit ist von einem
Austausch der Technik zur Darstellung der grafischen Oberfläche nur die oberste
Schicht betroffen. Eine Kernaufgabe der Service-Schicht des Clients ist die
dynamische Generierung der grafischen Benutzeroberfläche mithilfe des auf dem
Server hinterlegten Vokabulars.

Die Operations-Schicht des Client enthält \ua die Funktionseinheiten,
die die Kommunikation mit den beiden anderen Komponenten durchführen. Diese tun
nichts anderes als Daten von der Service-Schicht entgegen zu nehmen und sie an
die richtigen Prozeduren der richtigen Komponente weiterzuleiten. Somit hätte
eine Änderung der Kommunikationstechnik nur Auswirkungen auf die entsprechenden
Funktionseinheiten dieser Schicht.

Die zweite Komponente, der Mediator, verhindert, dass der Benutzer alle
Metadaten von Hand eingeben muss. Der Mediator soll automatisch mittels
verschiedener Werkzeuge Metadaten aus den digitalen Objekten des
Informationspakets extrahieren und die gewonnenen Metadaten zurück an
den Server senden. Dazu bedient er sich des Mediator-Wrapper-Architekturmusters
\sectionReference{sec:mediatorWrapper}.

Der Mediator enthält eine oberste Schicht namens Communication. Diese nimmt alle
Anfragen entgegen und ist somit die einzige Schicht, die direkt mit
der Kommunikationstechnik zu tun hat. Die Communication-Schicht leitet ihre
Daten wiederum an die Service-Schicht weiter, die wie bereits beschrieben auf
atomare Methoden der Operations-Schicht zurückgreift. Der Mediator kommuniziert
hauptsächlich mit dem Server; vom Client nimmt er nur Anfragen entgegen, welche
seine Konfiguration betreffen, da diese vom Benutzer über die grafische Oberfläche
des Clients vorgenommen wird. 

Eine Besonderheit des Mediators ist in dem bereits erwähnten
Mediator-Wrapper-Architekturmuster zu finden. Der Mediator kann auf
unterschiedliche Datenquellen zugreifen und Daten von diesen anfordern. Die
Datenquellen besitzen jedoch alle eine proprietäre \ac{API}. Wenn die
Service-Schicht direkt auf diese \acp{API} zugreifen würde, würde ein Austausch
einer Datenquelle oder eine Veränderung in der \ac{API} direkte Änderungen in
der Service-Schicht nach sich ziehen. Ebenso wäre es schwieriger, eine neue Datenquelle einzubinden.

Aus diesem Grund wird die proprietäre \ac{API} einer jeden Datenquelle in einen
sogenannten Wrapper \rede{eingepackt}. Alle Wrapper bieten eine einheitliche
Schnittstelle an, welche der Service-Schicht bekannt ist. Nur der Wrapper einer
bestimmten Datenquelle kennt deren proprietäre \ac{API}. Somit haben Änderungen
an der \ac{API} oder ein Austausch einer Datenquelle nur Änderungen an dem
jeweiligen Wrapper zur Folge. Soll eine neue Datenquelle eingebunden werden, muss nur ein
neuer Wrapper für diese Datenquelle implementiert und in der
Service-Schicht registriert werden.

Die dritte Komponente, der Server, enthält die Geschäftslogik der
Gesamt-An\-wen\-dung. Somit können vollkommen unterschiedliche Clients auf dieselbe
Geschäftslogik zugreifen, solange sie dieselben Schnittstellen verwenden.
Aufgrund seiner Komplexität ist der Server in mehrere Module mit jeweils einer klar
definierten Aufgabe unterteilt. Außerdem folgt fast jedes Modul der bereits 
beschriebenen Drei-Schichten-Architektur. Manche Module bestehen \NOT aus drei
Schichten, weil sie \zB keine Kommunikation mit der Außenwelt beinhalten, sondern 
nur intern genutzt werden.

Dabei nimmt auch hier nur die Communication-Schicht des jeweiligen Moduls die
Anfragen entgegen, reicht empfangene Daten an die Service-Schicht
weiter, welche wiederum auf die Operations-Schichten zugreifen. Zwischen den
Modulen können Abhängigkeiten bestehen. Dazu werden ganz klare Schnittstellen
definiert, so dass die Module, die von anderen Modulen abhängig sind, deren
Implementierungsdetails \NOT kennen.

Durch diesen modularen Aufbau ist es sehr einfach, ein ganzes Modul
auszutauschen ohne dass die restlichen Module durch die Änderung betroffen sind.
Einzelne Schichten oder Module lassen sich testen, Fehler lassen sich relativ
einfach eingrenzen. Da Abhängigkeiten zu Frameworks und Dritt-\acp{API} nur in
einer Komponente des Systems bestehen, ist ganz klar, welche Komponente ausgetauscht
werden muss, falls ein Framework ersetzt werden soll oder sich eine proprietäre
\ac{API} ändert. Alle Daten aus properitären \acp{API} werden von den
Operations-Schichten in Dateneinheiten umgewandelt, die unabhängig von einer
bestimmten \ac{API} sind.

Alle Module, die für andere Module zugreifbar sein sollen, stellen 
Fassaden zur Verfügung \sectionReference{sec:facade}. Für jede
Abhängigkeitsbeziehung zwischen Modulen existiert jeweils eine Fassade,
die nur die für diese Abhängigkeit unbedingt erforderlichen Prozeduren zur
Verfügung stellt. Die Fassaden folgen einem klaren Namensschema: zuerst
der Name des Moduls, das zugreift, dann der Name des Moduls, auf das zugegriffen
wird, und schließlich das Suffix \code{Facade}. 

Die Prozeduren der Fassaden delegieren nur an die entsprechenden Prozeduren
der Funktionseinheiten des Moduls. Somit haben Änderungen innerhalb eines Moduls
keine Auswirkungen auf die abhängigen Module, solange sich die Fassaden \NOT
verändern.

Somit bietet {\PACKAGINGTOOLKIT} eine flexible Architektur, die schnelle
Änderungen und Anpassungen erlaubt, wobei die Änderungen lokal begrenzt sind. Es
wäre sogar möglich, die Module des Servers auf mehrere physische Server
aufzuteilen. Dann müssten nur die entsprechenden Module verschoben und 
die Kommunikationsschnittstellen entsprechend angepasst werden.
