\subsection{Spring und \SPRINGBOOT}
\label{sec:springboot}
\begin{flushright}{\slshape    
    In 1996, the Java programming language was still a young, exciting,
    up-and-coming platform. ($\ldots$) Unlike any language before it, Java made
    it possible to write complex applications made up of discrete parts.}  \\
    \medskip --- Craig Walls \cite{walls.2007}
\end{flushright}

Eine Web-Anwendung setzt einen Server voraus, der mittels \ac{HTTP} gestellte Anfragen
entgegen nehmen und beantworten kann. Eine Installation, Konfiguration und Wartung
eines solchen Servers kann sehr aufwändig sein und genau an dieser Stelle setzen
Spring und das Unterprojekt SpringBoot an und bieten somit eine Antwort auf die
Forschungsfrage 2, welche sich auf eine leichte Konfigurierbarkeit des Server
bezieht. 

Im März 1998 ergänzte Sun die Programmiersprache Java um die erste Version
der \acs{EJB}-Spezifikation (Enterprise JavaBeans), welche die
Konzepte von Java auf Serverseite erweiterte, jedoch die Entwicklung von
Unternehmensanwendungen \NOT nennenswert vereinfachte - was das eigentliche
Ziel der Spezifikation gewesen war. Statt dessen setzte sich das Framework
Spring dieses Ziel, ein quelloffenes Framework, dessen Grundstein von Rod
Johnson in dem Buch \rede{Expert One-to-One: J2EE Design and Development}
gelegt wurde \bibReference{Johnson.2002}.

Spring möchte die Entwicklung von Java und JavaEE-Anwendungen
(Java Enterprise Edition) unter Förderung guter Programmierpraktiken
vereinfachen.
Zwar ist Spring unabhängig von \acs{JavaEE}; es lässt sich jedoch einfach in
JavaEE-Techniken integrieren \bibReference{OatesLanger.2007}. Ein großer Vorteil von Spring gegenüber 
\acs{EJB} ist es, dass Spring \NOT zwangsläufig einen
Java-Applikations-Server benötigt, sondern als leichtgewichtiges Framework auch
in einfachen JavaSE-Programmen (Java Standard Edition) verwendet werden kann.

Eine der wichtigsten Bestandteile von Spring ist die \ac{DI}. Damit
soll die enge Verdrahtung von Objekten, Objekterzeugung und der Implementierung
verringert werden. Im Vordergrund steht dabei die Entkopplung von Komponenten.
Das bedeutet, dass die Objekte selbst ihre Abhängigkeiten untereinander
\NOT kennen. Ein Objekt a weiß \NOT, dass es von einem Objekt b abhängig ist.
 Es kennt nur ein Interface, aber \NOT die konkrete Implementierung. Die
Abhängigkeit von der konkreten Implementierung ist nur dem Spring-Framework
bekannt und dieses kann dann als externe Instanz die Abhängigkeiten zwischen den
Objekten herstellen. 
% Es kann a die Abhängigkeit zu Objekt b mittels \ac{DI}
% injizieren. 
Bei einer Änderung der Abhängigkeiten muss nur die Konfiguration
innerhalb des Frameworks verändert werden und \NOT das Programm selbst, solange
sich die Schnittstellen \NOT verändern. Dadurch fördert Spring das Schreiben
von gut testbarem Code \bibReference{Katamreddy.2016}.

% \cite{Wolff.2010} nennt weitere Vorteile von Spring gegenüber \ac{EJB}. Spring
% ermöglicht den Einsatz von \ac{DI} bei beliebigen Javaklassen und
% bietet im Gegensatz zu \ac{EJB} eine Unterstützung für \ac{AOP}. 

% In den Anfangsjahren wurden die Abhängigkeiten zwischen den Objekten über reine
% \ac{XML}-Dateien konfiguriert. Die
% Konfiguration mittels \ac{XML} hat sich jedoch bei großen Projekten als schwerfällig
% und unübersichtlich erwiesen. Spring führte in den Folgejahren zuerst eine
% Konfigurationsmöglichkeit mittels Java-Annotationen ein und schließlich eine
% reine Java-Konfiguration, bei welcher die Konfiguration nur über Javaklassen
% durchgeführt wird. Die verschiedenen Konfigurationsansätze können sogar
% gleichzeitig in einer Anwendung verwendet werden, so dass bspw. in einem
% älteren Programm, das noch auf \ac{XML} aufbaut, für die Konfiguration von neuen
% Klassen die Java-Konfiguration verwendet werden kann (\cite{Katamreddy.2016}).
% 
% Die Java-Konfiguration, deren Verwendung von den Spring-Entwicklern empfohlen
% wird, bietet neben einer höheren Flexibilität noch weitere Vorteile wie \zB
% eine geringere Fehleranfälligkeit. Denn die Konfiguration erfolgt über die
% Verwendung ganz gewöhnlicher Java-Klassen, die mit entsprechenden
% Spring-Annotationen markiert sind. Der Entwickler kann folglich auf die
% Hilfswerkzeuge einer IDE zurückgreifen und der Compiler kann Fehler entdecken,
% die bei der \ac{XML}-Konfiguration nur schwer zu finden wären.

Um Spring herum hat sich inzwischen
ein ganzes Ökosystem aus verschiedenen Projekten entwickelt. Dieses Ökosystem
wird ständig um neue Projekte erweitert \bibReference{Wolff.2014}. Einige dieser
Projekte sind Spring \acs{LDAP} für den einfachen Zugriff auf
\ac{LDAP}-Systeme, Spring \ac{MVC}, welches die Entwicklung von \WEBAPPp
vereinfacht, Spring Security zur Absicherung von Java-Anwendungen und Internetseiten. Ein weiteres, relativ neues
Teilprojekt ist \SPRINGBOOT.

Wenn die Spring-Anwendung auf einem Server ausgeführt werden soll, muss der
Server normalerweise erst auf dem System, auf welchem die Anwendung ablaufen
soll, installiert und konfiguriert werden.
{\SPRINGBOOT} soll diesen Vorgang vereinfachen und bringt bereits einen integrierten {\WEBSERVER} mit. Das hat zur
Folge, dass keine aufwändige Konfiguration notwendig ist und dass
Kompatibilitätsprobleme zwischen Anwendung und Server praktisch ausgeschlossen
sind. Dabei basiert Spring Boot auf dem Prinzip \glqq Convention over
Configuration\grqq , was bedeutet, dass eine aufwendige Konfiguration entfällt
und stattdessen gültige Konventionen eingehalten werden sollen. Eine
Konfiguration ist nur notwendig, wenn der Programmierer die
Standardeinstellungen verändern möchte. 
% Normalerweise wird als
% Konfigurationsmethode die moderne Java-Konfiguration verwendet.

\cite{walls.2007} erklärt, dass Java-EE-Applikationsserver inzwischen \NOT mehr
den größten Marktanteil hätten, sondern einfache {\WEBSERVER} wie Tomcat, die
wesentlich einfacher in der Handhabung sind und einen geringeren
Ressourcenverbrauch aufweisen, allerdings auch weniger Funktionen anbieten.
Aber viele dieser Funktionen werden in der Praxis auch oft \NOT benötigt. An
dieser Stelle kommt der Vorteil von Spring zum Tragen, weil dessen
Programmiermodell im Gegensatz zu dem von \ac{EJB} auch auf einfache Umgebungen übertragen werden kann.

% Nach \cite{Katamreddy.2016} werden häufig sehr ähnliche Spring-Konfigurationen
% verwendet. So werden oft Datenquellen, Transaktionsmanager oder Server
% konfiguriert. {\SPRINGBOOT} stellt Standardkonfigurationen zur Verfügung, die bei
% Bedarf überschrieben und an die eigenen Bedürfnisse angepasst werden können.
% Dazu definiert {\SPRINGBOOT} die sogenannten Starter. Diese bezeichnen eine Menge
% von Abhängigkeiten, die automatisch vorkonfiguriert zum Projekt hinzugefügt
% werden.

% Die Klasse, welche die \code{main}-Methode enthält, muss mit der Annotation
% \code{@SpringBootApplication} markiert werden. Anschließend verwendet die
% Anwendung, sobald sie gestartet wird, automatisch den integrierten und
% konfigurierten Server. 
Erzeugt wird automatisch eine jar-Datei, der mit \NOT
ansieht, dass sie in Wirklichkeit eine auf einem Server basierende {\WEBAPP}
ist, und die einfach wie eine gewöhnliche JavaSE-Anwendung gestartet werden
kann. 