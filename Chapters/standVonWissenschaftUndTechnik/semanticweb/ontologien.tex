\subsection{Ontologien}
Ein semantisches Netz ist konzeptuell als Graph organisiert, in dem die
miteinander in Beziehung stehenden Informationen über Kanten 
verbunden sind. Das Kernelement sind Klassifikationssysteme, die mithilfe \sog
Ontologien definiert werden \bibReference{pi7.fernunihagen}. Ontologien können
auch verwendet werden, um Struktur unabhängig von einer bestimmten Domäne
festzulegen.
Somit können sie auch zur Beschreibung des Aufbaus von \acp{DMP} eingesetzt werden.

\begin{quote}
	\rede{Research on ontology is becoming increasingly widespread in the computer
	science community. While this term has been rather confined to the
	philosophical sphere in the past, it is now gaining a specific role in
	Artificial Intelligence, Computational Linguistics, and Database Theory.}
\end{quote}

Wie in diesem Zitat aus \cite{Guarino.1998} beschrieben, stammt der Begriff
\rede{Ontologie} (gr. ontos - Sein, logos - Lehre) ursprünglich aus der
Philosophie, gewinnt jedoch auch in Teilgebieten der Informatik immer mehr an
Bedeutung. \cite{UniMuenchenFakPhilosophie} beschreibt die Ontologien in der
Philosophie folgendermaßen:

\begin{quote}
	\rede{Die Ontologie beschäftigt sich mit allem, was es gibt, denn sie fragt
	erstens, was es heißt, daß es etwas gibt, und zweitens, welche Kategorien von Objekten 
	existieren und in welchem Verhältnis sie zueinander stehen.}
\end{quote}

Der Begriff der Ontologie wurde in der Informatik in das Gebiet der Künstlichen
Intelligenz übernommen und wird bei \cite{Jfsowa.2016} folgendermaßen definiert:

\begin{quote}
	\rede{The subject of ontology is the study of the categories of things that
	exist or may exist in some domain. The product of such a study, called an ontology, is a 
	catalog of the types of things that are assumed to exist in a domain of interest
	D from the perspective of a person who uses a language L for the purpose of
	talking  about D.}
\end{quote} 

Eine weitere, sehr häufig zitierte Definition findet sich bei
\cite{Gruber.1995}:

\begin{quote}
	\rede{An Ontology is an explicit specification of a conceptualisation.}
\end{quote}

Ontologien wurden \NOT ohne Grund von der Informatik aus der
Philosophie übernommen, denn in beiden Fachbereichen gibt es große Ähnlichkeiten
in der Interpretation dieses Begriffs. 
% Wichtig ist hierbei, dass sie sich mit
% den Kategorien von Objekten beschäftigt und deren Verhältnis zueinander. 
In der
Informatik geht es um die Kategorien, die in einem Anwendungsbereich, einer
Domäne, existieren. Die von \cite{Gruber.1995} erwähnte Konzeptualisierung
bezeichnet ein abstraktes Modell über die Vorstellung der Menschen über Dinge in der realen
Welt oder auch die subjektive Interpretation von realen Dingen in der Welt
\bibReference{Hemmje1874}. 

Eine Ontologie stellt ein Vokabular zur Beschreibung eines Wissensbereichs zur
Verfügung. Eine Ontologie sollte maschinenlesbar sein
und \NOT die gesamte Welt modellieren, sondern nur einen Ausschnitt, der den
gewünschten Wissensbereich umfasst.
Wie in der Philosophie stehen die Kategorien in der Informatik in Beziehung
zueinander. Diese Beziehungen zwischen den Konzepten ermöglichen logische
Schlussfolgerungen und automatisches Schließen auf \NOT vorhandene
Informationen durch Maschinen. 
% der für die entsprechende Aufgabe von Bedeutung ist.
% Das in ihr enthaltene Wissen sollte \NOT privat sein, sondern von
% einer Gruppe gemeinschaftlich genutzt werden. Zudem soll eine Ontologie \NOT
% die gesamte Welt modellieren, sondern nur einen Ausschnitt der Welt, der für die
% entsprechende Aufgabe von Bedeutung ist. 

% \cite{Hemmje1874} nennt vier Anwendungsgebiete für Ontologien:
% 
% \begin{itemize}
%   \item Ontologien ermöglichen ein gemeinsames Verständnis und eine gemeinsame
%   Kommunikation zwischen Menschen, obwohl sie verschiedene Ansichten von
%   Konzepten haben. Somit vereinfachen sie die Kommunikation.
%   \item Ontologien stellen ein standardisiertes Grundvokabular zur Verfügung.
%   Dieses kann in der IT verwendet werden, um Kompatibiltät und Interoperabilität
%   zwischen unterschiedlichen Systemen zu erreichen. Das durch die Ontologie
%   definierte Vokabular kann als Basis für die Anforderungsdefinition, die
%   Softwareentwicklung und die Dokumentation dienen. 
%   \item Ontologien können die Interoperabilität zwischen verschiedenen
%   Anwendungen vereinfachen. Möchten zwei Anwendungen miteinander kommunizieren,
%   müssen sie auf einer gleichen Semantik in ihren Konzepten aufbauen. Sie kann
%   die Interpretationsvorschrift für die Daten selbst mitbringen oder sie werden
%   in Form einer für beide Seiten zugänglichen Ontologie mitgeliefert.
%   \item Ontologien ermöglichen das automatische Schließen, \dahe Programme
%   können aufgrund von Ableitungsregeln logische Schlüsse ziehen. Diese Ableitungsregeln
%   sind formal in der Ontologie festgelegt. 
% \end{itemize}

% Ein wichtiges Anwendungsgebiet für Ontologien ist das Semantische Netz. 

Berners-Lee nennt Ontologien als eine wichtige Komponente des Semantischen Netzes, da mit ihnen für
Maschinen verständliche Schlussfolgerungsregeln formuliert werden können
\bibReference{BernersLee.1996}.

% , das Tim
% Berners-Lee in \cite{BernersLee.2001} beschreibt. Dabei geht es um eine
% Anreicherung des bestehenden Internets mit Semantik. Diese zusätzliche Semantik
% erlaubt es, Informationen mit maschinenlesbaren und durch Maschinen auswertbaren
% Bedeutungsinhalten zu versehen. Dazu wird eine Sprache benötigt, die sowohl Daten enthält als auch
% Regeln zum Schlussfolgern, damit Maschinen \NOT vorhandene Informationen
% selbstständig erschließen können. 

Ontologien sind somit formale Darstellungen einer Menge von Begriffen und der
Beziehungen zwischen ihnen innerhalb einer bestimmten Domäne. Mit ihnen ist es
möglich, Wissen zwischen Programmen und Diensten auszutauschen. Sie enthalten
auch Regeln zur Schlussfolgerung (Inferenz), die neben dem automatischen
Schließen auch die Überprüfung auf Gültigkeit ermöglichen. Ontologien können
\zB dazu dienen, bereits existierende Wissenbestände zusammenzuführen oder
Wissen in bestehende Wissensbestände zu integrieren.

Der Unterschied zur klassischen Datenbank besteht darin, dass \NOT nur Daten
gespeichert werden, sondern auch Informationen über den Zusammenhang und formale
Beschreibungen der Daten. Mithilfe dieser Regeln ist es möglich, dass ein Programm Schlüsse aus Daten zieht,
Widersprüche erkennt oder sogar fehlendes Wissen selbstständig ergänzt.