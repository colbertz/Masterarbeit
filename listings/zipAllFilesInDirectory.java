public String zipAllFilesInDirectory(final String directoryPath, 
								     final String filenameWithoutSuffix) {
	byte[] buffer = new byte[PackagingToolkitConstants.BITS_OF_ONE_BYTE];
	
	File directory = new File(directoryPath); 		
	String zipFilename = directory.getAbsolutePath() + File.separator + filenameWithoutSuffix + 
			PackagingToolkitFileUtils.ZIP_FILE_EXTENSION;
	
	try (FileOutputStream fileOutputStream = new FileOutputStream(zipFilename);
		 ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream)) {
		File[] files = directory.listFiles();
		if (files != null) {
			for (File fileToZip: files) {
				if (!fileToZip.getName().contains(filenameWithoutSuffix)) {
					ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
					zipOutputStream.putNextEntry(zipEntry);
					FileInputStream fileInputStreamToZip = new FileInputStream(fileToZip);
					
					int length = 0;
					while ((length = fileInputStreamToZip.read(buffer)) > 0) {
						zipOutputStream.write(buffer, 0, length);
					}
					
					fileInputStreamToZip.close();
				}
			}
		}
		return zipFilename;
		
	} catch(IOException ex) {
		throw PackagingException.getZipFileCouldNotBeCreated(zipFilename);
	}
}