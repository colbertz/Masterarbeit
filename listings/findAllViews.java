public ViewListResponse findAllViews() {
	ViewListResponse viewListResponse = ResponseModelFactory.getViewListResponse();
	
	for(ViewEntity view: viewRepository.findAll()) {
		String viewName = view.getViewName();
		int viewId = view.getViewId();
		ViewResponse viewResponse = ResponseModelFactory.getViewResponse(viewName, viewId);
		
		for(OntologyClassEntityHibernateImpl ontologyClassEntity: view.getOntologyClassEntities()) {
			String localName = ontologyClassEntity.getClassName();
			String namespace = ontologyClassEntity.getNamespace();
			OntologyClassResponse ontologyClassResponse = ResponseModelFactory.
				getOntologyClassResponse(namespace, localName);
			viewResponse.addOntologyClass(ontologyClassResponse);
		}
		
		viewListResponse.addViewToList(viewResponse);
	}
	
	return viewListResponse;
}