public InputStream downloadVirtualArchive(ArchiveResponse archiveResponse, String serverUrl) {
	try {
		String uuid = archiveResponse.getUuid();
		String targetUrl = serverUrl + PathConstants.SERVER_PACKAGING_INTERFACE + 
			PathConstants.SERVER_DOWNLOAD_VIRTUAL_ARCHIVE;
		targetUrl = targetUrl.replace("{uuid}", uuid);
		RestTemplate restTemplate = getRestTemplate();
		restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());
		HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
	
		MultiValueMap<String, String> parameterMap = new LinkedMultiValueMap<>();
		parameterMap.add(ParamConstants.PARAM_UUID, uuid);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    
	    ResponseEntity<byte[]> response = restTemplate.exchange(targetUrl, HttpMethod.GET, 
	    	entity, byte[].class);
	    if (response.getStatusCode() == HttpStatus.OK) {
	    	byte[] fileContent = response.getBody();
	    	return new ByteArrayInputStream(fileContent);
	    }
	    
	    return null;
	} catch (ResourceAccessException exception) {
		ConnectionException connectionException = ConnectionException.createServerConnectionError(serverUrl);
		throw connectionException;
	}
}