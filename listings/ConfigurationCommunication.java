@RestController
@RequestMapping(PathConstants.SERVER_CONFIGURATION_INTERFACE + "/**")
public class ConfigurationCommunication {
	@RequestMapping(value = PathConstants.SERVER_GET_CONFIGURATION_DATA, method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<ServerConfigurationDataResponse> readConfigurationData() { 
		final ServerConfigurationDataResponse configurationData = configurationService.readServerConfiguration();
		return new ResponseEntity<ServerConfigurationDataResponse>(configurationData, HttpStatus.OK);
	}
}