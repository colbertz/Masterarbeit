private Model convertDataPropertiesToModel(TripleStatementCollection
	tripleStatements) { 
	
	Model model  = new LinkedHashModel();
	
	while(tripleStatements.hasNextLiteralStatement()) {
		TripleStatement tripleStatement = tripleStatements.getNextLiteralStatement();
		TripleSubject tripleSubject = tripleStatement.getSubject();
		String iriOfSubjectAsString = tripleSubject.getIri();
		IRI iriOfSubject = valueFactory.createIRI(iriOfSubjectAsString);
		
		TriplePredicate triplePredicate = tripleStatement.getPredicate();
		String iriOfPredicateAsString = triplePredicate.getIri();
		IRI iriOfPredicate = valueFactory.createIRI(iriOfPredicateAsString);
		
		TripleObject tripleObject = tripleStatement.getObject();
		Literal literalOfObject = valueFactory.createLiteral(tripleObject.getValue().toString());
	
		Statement statement = valueFactory.createStatement(iriOfSubject, iriOfPredicate, literalOfObject);
		model.add(statement);
	}
	
	return model;
} 