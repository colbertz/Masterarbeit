public abstract class AbstractList implements ListInterface {
	private List<Object> objectList;
	
	protected void add(final Object element) {
		ListValidator.checkListElement(element);
		objectList.add(element);
	}
	
	@Override
	protected Object getElement(final int index) {
		ListValidator.checkIndex(index, objectList);
		return objectList.get(index);
	}
	
	@Override
	public int getSize() {
		return objectList.size();
	}
}