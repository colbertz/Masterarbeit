public void addTriplesToStore(TripleStatementCollection tripleStatements,
		WorkingDirectory workingDirectory) { 
	deleteStatementsIfExist(tripleStatements, workingDirectory);
	
	Model model = convertTripleStatementsToModel(tripleStatements);
	Repository repository = initializeRepository(workingDirectory);
		
	try (RepositoryConnection connection = repository.getConnection();) {
		connection.add(model);
	} catch (RDFParseException e) {
		e.printStackTrace();
	} finally {
		repository.shutDown();
	}
}