@Override
public OntologyClass getHierarchy(final String informationPackageClassName) {
	final OntClass informationPackageClass = searchClass(informationPackageClassName);
	final ExtendedIterator<OntClass> iterator = informationPackageClass.
			listSubClasses();
	final String rootLocalName = informationPackageClass.getLocalName();
	final String namespace = informationPackageClass.getNameSpace();
	final OntologyClass rootClass = OntologyModelFactory.createOntologyClass(namespace, rootLocalName);
	
	while(iterator.hasNext()) {
		final OntClass ontClass = iterator.next();
		rootClass.addSubClass(getHierarchy(ontClass, rootClass));
	}
	
	return rootClass;
}

private OntologyClass getHierarchy(final OntClass ontClass, 
    							   final OntologyClass ontologyClass)  {
	final OntologyClass childOntologyClass = OntologyModelFactory.createOntologyClass
				(ontClass.getNameSpace(), 
				 ontClass.getLocalName());
	for (Iterator<OntClass> iterator = ontClass.listSubClasses(); iterator.hasNext();) {
    	final OntClass child = iterator.next();
    	childOntologyClass.addSubClass(getHierarchy(child, childOntologyClass));
    }
	return childOntologyClass,
}