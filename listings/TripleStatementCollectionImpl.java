public class TripleStatementCollectionImpl extends
		AbstractList implements TripleStatementList, Serializable { 
	private LiteralStatementsIterator literalStatementsIterator;
	
	@Override
	public void addTripleStatement(TripleStatement tripleStatement) {
		super.add(tripleStatement);
	}
	
	@Override
	public TripleStatement getTripleStatement(int index) {
		return (TripleStatement)super.getElement(index);
	}
	
	@Override
	public boolean hasNextLiteralStatement() {
		return literalStatementsIterator.hasNext();
	}
	
	@Override
	public TripleStatement getNextLiteralStatement() {
		return literalStatementsIterator.next();
	}
}