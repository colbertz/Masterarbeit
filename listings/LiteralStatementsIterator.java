private class LiteralStatementsIterator implements Iterator<TripleStatement> {
	private TripleStatement currentStatement;
	private int counter;
	
	public LiteralStatementsIterator() {
		counter = 0;
	}
	
	@Override
	public boolean hasNext() {
		int size = getTripleStatementsCount();
		if (counter < size) {
			for (int i = counter; i < size; i++) {
				TripleStatement tripleStatement = getTripleStatement(i);
				if (tripleStatement.isLiteralStatement()) {
					currentStatement = tripleStatement;
					counter = i + 1;
					return true;
				}
			}
			return false;
		} else {
			return false;
		}
	}

	@Override
	public TripleStatement next() {
		if (currentStatement == null) {
			currentStatement = getTripleStatement(0);
		}
		
		if (hasNext()) {
			return currentStatement;
		} else {
			return null;
		}
		
	}
}